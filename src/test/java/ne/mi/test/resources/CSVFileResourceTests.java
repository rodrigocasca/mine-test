package ne.mi.test.resources;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import ne.mi.test.AppApplication;
import ne.mi.test.pojo.FileCSVResponse;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CSVFileResourceTests {

	@Autowired
    private TestRestTemplate testRestTemplate;

	//@Test
    public void callUploadWithNullFileShouldReturnBadRequest() {

		HttpStatus statusCode = this.testRestTemplate.postForEntity("/upload", null, FileCSVResponse.class).getStatusCode();
		assertThat(statusCode, is(400));

	}

}
