package ne.mi.test.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.io.input.BOMInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import ne.mi.test.model.CSVRequest;
import ne.mi.test.model.CSVResponse;
import ne.mi.test.model.CSVResponse.Builder;

@Service
public class CSVFileService {

	private static final Logger log = LoggerFactory.getLogger(CSVFileService.class);

    public CSVResponse process(final CSVRequest request) {
    	Assert.notNull(request, "request must not be null");

        log.info("Received CSV File");

        ByteArrayInputStream bis = new ByteArrayInputStream(request.getContent().toByteArray());

        Reader in = null;
		CSVParser parser = null;
		List<String> header = new ArrayList<>();

		try {
			in =  new InputStreamReader(new BOMInputStream(bis), "UTF-8");
			parser = new CSVParser(in, CSVFormat.EXCEL.withHeader());

			header = parser.getHeaderNames();
			log.info("header: {}", parser.getHeaderNames().toString());
		} catch (Exception e) {
			log.error("Exception: " + e.getMessage());
		} finally {
			try {
				if (Objects.nonNull(in)) {
					in.close();
				}

				if (Objects.nonNull(parser)) {
					parser.close();
				}
			} catch (IOException e) {
				log.error("IO Exception closing parser: " + e.getMessage());
			}
		}

		Builder responseBuilder = CSVResponse.newBuilder();
		if (Objects.nonNull(header) && header.size() > 0) {

			header.forEach(h -> {
				log.info("Adding header: {}", h);
				responseBuilder.addHeader(h);
			});
		}
        return responseBuilder.build();
    }

}
