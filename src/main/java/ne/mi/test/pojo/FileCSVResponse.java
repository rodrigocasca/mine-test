package ne.mi.test.pojo;

import java.util.List;

public class FileCSVResponse {

	private List<String> header;

	public FileCSVResponse(final List<String> header) {
		super();
		this.header = header;
	}

	public List<String> getHeader() {
		return this.header;
	}

}
