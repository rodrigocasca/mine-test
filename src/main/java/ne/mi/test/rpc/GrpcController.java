package ne.mi.test.rpc;

import org.lognet.springboot.grpc.GRpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import io.grpc.stub.StreamObserver;
import ne.mi.test.model.CSVRequest;
import ne.mi.test.model.CSVResponse;
import ne.mi.test.model.CoreServiceGrpc;
import ne.mi.test.service.CSVFileService;

@GRpcService
public class GrpcController extends CoreServiceGrpc.CoreServiceImplBase {

	private static final Logger log = LoggerFactory.getLogger(GrpcController.class);

	@Autowired
	private CSVFileService service;

	@Override
	public StreamObserver<CSVRequest> cSVFile(final StreamObserver<CSVResponse> responseObserver) {

		return new StreamObserver<CSVRequest>() {
			CSVResponse response = null;
			@Override
			public void onNext(final CSVRequest req) {
				try{
					log.info("Request: {}", req.getContent().toStringUtf8());
					this.response = GrpcController.this.service.process(req);
				} catch (Exception e) {
					log.error("Exception reading data file [{}]", e.getMessage());
				}
			}
			@Override
			public void onError(final Throwable t) {
				log.error("CSV File error. Message: [{}]", t.getMessage());
			}
			@Override
			public void onCompleted() {
				log.info("Response content size: {}", this.response.getHeaderCount());
				this.response.getHeaderList().forEach(c -> log.info("Content: {}", c));

				responseObserver.onNext(this.response);
				responseObserver.onCompleted();
			}
		};
	}


}