package ne.mi.test.model;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.21.0)",
    comments = "Source: csv.proto")
public final class CoreServiceGrpc {

  private CoreServiceGrpc() {}

  public static final String SERVICE_NAME = "ne.mi.grpc.core.CoreService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<ne.mi.test.model.CSVRequest,
      ne.mi.test.model.CSVResponse> getCSVFileMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "CSVFile",
      requestType = ne.mi.test.model.CSVRequest.class,
      responseType = ne.mi.test.model.CSVResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<ne.mi.test.model.CSVRequest,
      ne.mi.test.model.CSVResponse> getCSVFileMethod() {
    io.grpc.MethodDescriptor<ne.mi.test.model.CSVRequest, ne.mi.test.model.CSVResponse> getCSVFileMethod;
    if ((getCSVFileMethod = CoreServiceGrpc.getCSVFileMethod) == null) {
      synchronized (CoreServiceGrpc.class) {
        if ((getCSVFileMethod = CoreServiceGrpc.getCSVFileMethod) == null) {
          CoreServiceGrpc.getCSVFileMethod = getCSVFileMethod =
              io.grpc.MethodDescriptor.<ne.mi.test.model.CSVRequest, ne.mi.test.model.CSVResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "ne.mi.grpc.core.CoreService", "CSVFile"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ne.mi.test.model.CSVRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  ne.mi.test.model.CSVResponse.getDefaultInstance()))
                  .setSchemaDescriptor(new CoreServiceMethodDescriptorSupplier("CSVFile"))
                  .build();
          }
        }
     }
     return getCSVFileMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static CoreServiceStub newStub(final io.grpc.Channel channel) {
    return new CoreServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static CoreServiceBlockingStub newBlockingStub(
      final io.grpc.Channel channel) {
    return new CoreServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static CoreServiceFutureStub newFutureStub(
      final io.grpc.Channel channel) {
    return new CoreServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class CoreServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public io.grpc.stub.StreamObserver<ne.mi.test.model.CSVRequest> cSVFile(
        final io.grpc.stub.StreamObserver<ne.mi.test.model.CSVResponse> responseObserver) {
      return asyncUnimplementedStreamingCall(getCSVFileMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getCSVFileMethod(),
            asyncClientStreamingCall(
              new MethodHandlers<
                ne.mi.test.model.CSVRequest,
                ne.mi.test.model.CSVResponse>(
                  this, METHODID_CSVFILE)))
          .build();
    }
  }

  /**
   */
  public static final class CoreServiceStub extends io.grpc.stub.AbstractStub<CoreServiceStub> {
    private CoreServiceStub(final io.grpc.Channel channel) {
      super(channel);
    }

    private CoreServiceStub(final io.grpc.Channel channel,
        final io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CoreServiceStub build(final io.grpc.Channel channel,
        final io.grpc.CallOptions callOptions) {
      return new CoreServiceStub(channel, callOptions);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<ne.mi.test.model.CSVRequest> cSVFile(
        final io.grpc.stub.StreamObserver<ne.mi.test.model.CSVResponse> responseObserver) {
      return asyncClientStreamingCall(
          getChannel().newCall(getCSVFileMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   */
  public static final class CoreServiceBlockingStub extends io.grpc.stub.AbstractStub<CoreServiceBlockingStub> {
    private CoreServiceBlockingStub(final io.grpc.Channel channel) {
      super(channel);
    }

    private CoreServiceBlockingStub(final io.grpc.Channel channel,
        final io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CoreServiceBlockingStub build(final io.grpc.Channel channel,
        final io.grpc.CallOptions callOptions) {
      return new CoreServiceBlockingStub(channel, callOptions);
    }
  }

  /**
   */
  public static final class CoreServiceFutureStub extends io.grpc.stub.AbstractStub<CoreServiceFutureStub> {
    private CoreServiceFutureStub(final io.grpc.Channel channel) {
      super(channel);
    }

    private CoreServiceFutureStub(final io.grpc.Channel channel,
        final io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CoreServiceFutureStub build(final io.grpc.Channel channel,
        final io.grpc.CallOptions callOptions) {
      return new CoreServiceFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_CSVFILE = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final CoreServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(final CoreServiceImplBase serviceImpl, final int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(final Req request, final io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (this.methodId) {
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        final io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (this.methodId) {
        case METHODID_CSVFILE:
          return (io.grpc.stub.StreamObserver<Req>) this.serviceImpl.cSVFile(
              (io.grpc.stub.StreamObserver<ne.mi.test.model.CSVResponse>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class CoreServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    CoreServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return ne.mi.test.model.CoreGrpcApi.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("CoreService");
    }
  }

  private static final class CoreServiceFileDescriptorSupplier
      extends CoreServiceBaseDescriptorSupplier {
    CoreServiceFileDescriptorSupplier() {}
  }

  private static final class CoreServiceMethodDescriptorSupplier
      extends CoreServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    CoreServiceMethodDescriptorSupplier(final String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(this.methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (CoreServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new CoreServiceFileDescriptorSupplier())
              .addMethod(getCSVFileMethod())
              .build();
        }
      }
    }
    return result;
  }
}
