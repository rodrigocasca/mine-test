package ne.mi.test.rest;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Objects;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.io.input.BOMInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ne.mi.test.pojo.FileCSVResponse;

/**
 * Controller for upload CSV files.
 *
 * @author rodrigo.cascarrolho
 *
 */
@RestController
public class FileCSVController {

	private static final Logger log = LoggerFactory.getLogger(FileCSVController.class);

	@RequestMapping(
			path = "/upload",
			method = RequestMethod.POST,
			produces = "application/json")
	public ResponseEntity<FileCSVResponse> upload(
	        @RequestParam("file") final MultipartFile file) throws IOException {
		if (Objects.isNull(file)) {
			log.error("BAD REQUEST: file is null");
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Reader in = null;
		CSVParser parser = null;
		try {
			log.info("File received: " + file.getName());

			log.info("Parsing CSV file...");
			in =  new InputStreamReader(new BOMInputStream(file.getInputStream()), "UTF-8");
			parser = new CSVParser(in, CSVFormat.EXCEL.withHeader());

			log.info("Creating response...");
			FileCSVResponse csv = new FileCSVResponse(parser.getHeaderNames());
			return new ResponseEntity<>(csv, HttpStatus.OK);

		} catch (Exception e) {
			log.error("Exception: " + e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			if (Objects.nonNull(in)) {
				in.close();
			}

			if (Objects.nonNull(parser)) {
				parser.close();
			}
		}
	}

}
